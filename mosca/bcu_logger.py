import logging 
from logging.handlers import RotatingFileHandler
import time
import os

class bcu_logger(logging.Logger):
    rotating_maxbytes = 1000000
    rotating_backups = 4
    default_level = logging.WARNING
    default_level = logging.DEBUG

    def __init__(self,*args):
        super(bcu_logger,self).__init__(*args)
        self.log_filename = None
        self.log_fileh = None

        self.stdh = logging.StreamHandler()

        self.formatter = bcu_log_formatter()
        self.stdh.setFormatter(self.formatter) 
        self.set_level(self.default_level)

    def set_level(self, level):
        self.setLevel(level)

    def enable_stdout(self):
        self.addHandler(self.stdh)

    def disable_stdout(self):
        self.removeHandler(self.stdh)

    def disable_file(self,filename):
        if self.log_fileh:
           self.removeHandler(self.log_fileh)

    def enable_file(self,filename):
        if filename != self.log_filename:
           if self.log_fileh:
               self.removeHandler(self.log_fileh)

           self.log_fileh = RotatingFileHandler(filename, 
                                            maxBytes=self.rotating_maxbytes,
                                            backupCount=self.rotating_backups)
           self.addHandler(self.log_fileh)
           self.log_fileh.setFormatter(self.formatter) 
           self.log_filename = filename


class bcu_log_formatter(logging.Formatter):

    def format(self, record):
        millis = int(round(record.created%1 * 1000))
        strtime = time.strftime("%H:%M:%S",time.localtime(record.created))
        strtime += ".%03d" % millis
        basefile = os.path.basename(record.pathname)

        level = record.levelname
        levelno = record.levelno
        lineno = record.lineno
        funcname = record.funcName

        msg = record.msg

        fileinfo = "%s:%s (line:%s)" % (basefile,funcname,lineno)
        logline = "%s - % 8s - %-50s  | %s" % (strtime, level, fileinfo,msg)

        return logline

