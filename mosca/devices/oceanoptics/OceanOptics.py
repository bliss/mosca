
from tango.server import attribute, device_property
from .OceanDevice import OceanDevice

from mosca.Spectrometer import TriggerMode
from mosca.BaseSpectroDS import BaseSpectroDS, run_server

class OceanOptics(BaseSpectroDS):

    serial_no = device_property(dtype=str)
    default_exposure_time = device_property(dtype=float, default_value=100)
    margins_data=device_property(dtype=[int], default_value=[10,-10])

    # even when using trigger mode SYNC the exposure time decides on the
    #  actual exposure 
    default_readout_time = device_property(dtype=float, default_value=0)

    multichannel = False

    def init_device(self):

        self._trigger_modes = [TriggerMode.SOFTWARE, TriggerMode.SYNC]

        if self.serial_no:
            self.o_dev = OceanDevice(serialno=self.serial_no)
        else:
            self.o_dev = OceanDevice()

        if self.o_dev.is_connected():
            self.set_idle()
            self.o_dev.set_exposure_time(self.default_exposure_time)
            self.o_dev.set_number_frames(1)
            self.o_dev.set_trigger_mode(TriggerMode.SOFTWARE)

        BaseSpectroDS.init_device(self)

        self.o_dev.set_data_margins(*self.margins_data)
        self.o_dev.set_readout_time(self.default_readout_time)

    def always_executed_hook(self):
        self.update_state()

    def update_state(self):
        if self.o_dev.is_running():
            self.set_running()
        elif self.o_dev.is_idle():
            self.set_idle()
        else:
            self.set_fault()

    @attribute(dtype=[int], max_dim_x=2)
    def data_margins(self):
        return self.o_dev.get_data_margins()

    @data_margins.write
    def data_margins(self, value):
        if len(value) == 1: 
            return self.o_dev.set_data_margins(value[0])
        else:
            return self.o_dev.set_data_margins(value[0], value[1])

    @attribute(dtype=float)
    def temperature(self):
        return self.o_dev.get_temperature()

    # BEGIN implementation of functions required by BaseSpectroDS

    def get_spectrum_size(self):
        return self.o_dev.get_spectrum_size()

    def get_number_points(self):
        return self.o_dev.get_number_frames()

    def set_number_points(self, value):
        self.o_dev.set_number_frames(value)

    def get_acquired_points(self):
        return self.o_dev.get_current_frame()

    def get_model(self):
        return self.o_dev.get_model()

    def get_info(self):
        return self.o_dev.get_info()

    def get_preset_value(self):
        return self.o_dev.get_exposure_time()

    def set_preset_value(self, value):
        """
        <value> : exposure time in ms
        """
        self.o_dev.set_exposure_time(value)

    def get_trigger_mode(self):
        return self.o_dev.get_trigger_mode()

    def set_trigger_mode(self, trig_mode):
        self.o_dev.set_trigger_mode(trig_mode)

    def get_readout_time(self):
        return self.o_dev.get_readout_time()

    def set_readout_time(self, value):
        self.o_dev.set_readout_time(value)

    def prepare_acquisition(self):
        pass

    def start_acquisition(self):
        self.set_running()
        self.o_dev.start_acquisition()

    def abort_acquisition(self):
        self.o_dev.stop_acquisition()

    def stop_acquisition(self):
        self.o_dev.stop_acquisition()

    # END of implementation of functions required by BaseSpectroDS

def run_it():
    run_server(OceanOptics)

if __name__ == "__main__":
    run_it()
