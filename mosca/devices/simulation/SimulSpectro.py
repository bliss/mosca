
from mosca.Spectrometer import SpectroDataHandler
from mosca.Spectrometer import TriggerMode
import mosca
import threading
import time
import os
import numpy as np

datsize = 1024
datdir = os.path.join(os.path.dirname(__file__), 'data')

class SimulSpectro:
    default_nb_modules = 5

    mapping_mode = property(lambda self: self._mapping_mode)

    def __init__(self):

        self._map_buffers = None
        self._mapping_mode = 1
        self._map_npix = 1
        self._map_current_buffer_idx = 0
        self._map_current_pix = -1

        self.data_handler = SpectroDataHandler()
        self.data_handler.set_metadata_labels(['elapsed_ms', 'realt_ms', 'livetime']) 

        # self.version = mosca.get_version()
        self.version = 'sim1.1.1'
        self._model = "Simul MoSCa"
        self._info = f"simulated MoSCa spectrometer. version is {self.version}"

        # set defaults
        self._acquiring = False
        self._acquired_frames = 0
        self._preset_value = 100
        self._number_frames = 1
        self._readout_time = 0
        self._trigger_mode = TriggerMode.SOFTWARE

        # init size and load simulated data
        self.spectrum_size = 1024
        self.add_random = False

        self._low_value = 24
        self._high_value = 480

        self.preset_value = self._preset_value

        self.number_modules = self.default_nb_modules

        self._simul_spectra = None

    @mapping_mode.setter
    def mapping_mode(self, mapping_mode):
        if self.is_acquiring():
            raise RuntimeError("Cannot set mapping mode while "
                               "an acquisition is running.")
        self._mapping_mode = mapping_mode

    @property
    def number_modules(self):
        return self._nb_modules 

    @number_modules.setter
    def number_modules(self, nb):
        self._nb_modules = nb
        if nb > 1:
            self.data_handler.multichannel = True
        else:
            self.data_handler.multichannel = False

    @property
    def acquired_frames(self):
        return self._acquired_frames

    @property
    def preset_value(self):
        return self._preset_value

    @preset_value.setter
    def preset_value(self, value):
        self._preset_value = value
        self._exposure_time = (self._preset_value - self._readout_time)/1000.0

    @property
    def trigger_mode(self):
        return self._trigger_mode

    @trigger_mode.setter
    def trigger_mode(self, mode):
        self._trigger_mode = mode

    @property
    def readout_time(self):
        return self._readout_time

    @readout_time.setter
    def readout_time(self, value):
        self._readout_time = value
        self._exposure_time = (self._preset_value - self._readout_time)/1000.0

    @property
    def model(self):
        return self._model

    @property
    def spectrum_size(self):
        return self._spectrum_size

    @spectrum_size.setter
    def spectrum_size(self,value):
        self._spectrum_size = value
        self.simul_data = None
        # self.load_simul_data()

    @property
    def add_random(self):
        return self._add_random

    @add_random.setter
    def add_random(self,value):
        self._add_random = value

    @property
    def number_frames(self):
        return self._number_frames

    @number_frames.setter
    def number_frames(self,value):
        self._number_frames = value

    def is_multichannel(self):
        return self._nb_modules > 1

    def is_acquiring(self):
        return self._acquiring

    def get_info(self):
        return self._info

    def get_model(self):
        return self._model

    def prepare_acquisition(self):
        pass

    def start_acquisition(self):
        self._acquiring = True
        self._acquired_frames = 0

        data_info = {}
        data_info['preset_value'] = self._preset_value
        data_info['exptime'] = self._exposure_time
        data_info['model'] = self._model

        self.data_handler.init_data(self._spectrum_size, \
                                    nb_chans=self._nb_modules, data_info=data_info)

        self._stop_it = False
        if self._mapping_mode:
            self._map_buffers = [np.zeros((self.number_modules, self._map_npix, self._spectrum_size)),
                                 np.zeros((self.number_modules, self._map_npix, self._spectrum_size))]
            self._map_meta_buffers = [np.zeros((self.number_modules, self._map_npix, self.data_handler.metadata_size)),
                                      np.zeros((self.number_modules, self._map_npix, self.data_handler.metadata_size))]
            self._map_current_buffer_idx = 0
            self._map_current_pix = -1

        self.th1 = threading.Thread(target=self.acquisition_loop, daemon=True)
        self.th1.start()

    def abort_acquisition(self):
        self.stop_acquisition()

    def stop_acquisition(self):
        self._stop_it = True

    def acquisition_loop(self):
        refresh_time = 0.02

        t0 = time.perf_counter()
        self._map_current_pix = -1
        self._map_current_buffer_idx = -1

        while self._acquired_frames < self._number_frames and not self._stop_it:
            t0 = time.perf_counter() 

            self._map_current_pix += 1

            # initialize at the beginning of frame
            # dshape = (1, self._spectrum_size)
            # mshape = (1, len(self.data_handler.get_metadata_labels()))
            # print(f' Meta shape is {mshape}')
            # if self.is_multichannel():
            #     dshape = (self._nb_modules, ) + dshape
            #     mshape = (self._nb_modules, ) + mshape

            # self.data_handler.add_spectra(np.zeros(dshape), metadata=np.zeros(mshape))
                
            # simulate exposure with data live update
            t1 = time.perf_counter() 
            while not self._stop_it:
                elapsed = time.perf_counter() - t1

                if elapsed + refresh_time  >= self._exposure_time:
                    time.sleep(max(self._exposure_time - elapsed, 0))
                    break

                time.sleep(refresh_time)

            # after exposure is finished prepare data for each module

            data = None

            for modno in range(self._nb_modules):
                onedata = self._get_simul_data(modno, frame=self._acquired_frames)
                
                elapsed_ms = int((time.perf_counter() - t0)*1000)
                realt_ms = int((time.perf_counter() - t1)*1000)
                livetime = np.random.randint(80,95) # random number between 80 and 95
                onemeta = np.array([elapsed_ms, realt_ms, livetime])

                if self._mapping_mode:
                    buffer = self._map_buffers[self._map_current_buffer_idx]
                    metadata = self._map_meta_buffers[self._map_current_buffer_idx]
                    # print(buffer.shape, metadata.shape, modno, self._map_current_pix)
                    buffer[modno, self._map_current_pix, :] = onedata
                    metadata[modno, self._map_current_pix, :] = onemeta
                else:
                    if data is None:
                        data = onedata
                        metadata = onemeta
                    else:
                        data = np.vstack((data,onedata))
                        metadata = np.vstack((metadata,onemeta))

            self._acquired_frames += 1
            if self._mapping_mode:
                if self._map_current_pix + 1 == self._map_npix:
                    data = self._map_buffers[self._map_current_buffer_idx]
                    metadata = self._map_meta_buffers[self._map_current_buffer_idx]
                    self.data_handler.add_spectra(data, metadata=metadata)
                    self._map_current_buffer_idx = (self._map_current_buffer_idx + 1) % 2
                    self._map_current_pix = -1
            else:
                self.data_handler.add_spectra(data, metadata=metadata)

        self.stop_acquisition()
        self._acquiring = False
        print("IS ACQ OFF", self._acquiring, self._acquired_frames)

    def set_random_seed(self, seed):
        np.random.seed(seed)

    def load_simul_data(self):
        #print(f'Loading simulated DATA from directory: {datdir}')
        # FIXME: This have to use "import resources" instead
        filename = f'simul_dat{self._spectrum_size}.npy'
        self.simul_data = np.load(os.path.join(datdir,filename))
        #print(f'   simul_data. shape is: {self.simul_data.shape} sum value={self.simul_data.sum()}')

    def _get_simul_data(self, modno, frame=None):
        if self._simul_spectra is None:
            if self.simul_data is None:
                self.load_simul_data()
            # TODO: simul_data is uint64 -> adding 1+ modno/10 wont do anything
            data = self.simul_data * (1 + modno/10)
            if self._add_random:
                data += np.random.random(self._spectrum_size) * np.max(data) * 0.1 # add 10 % noise
            return data.astype('int64') 
        else:
            spectra = self._simul_spectra
            if modno >= spectra.shape[0]:
                modno = modno % spectra.shape[0]
            if frame >= spectra.shape[1]:
                frame = frame % spectra.shape[1]
            return spectra[modno, frame, :]

    def inject_simul_spectra(self, spectra):
        if spectra is not None:
            self.simul_data = None
            spectra.shape = self.number_modules, -1, self.spectrum_size
            self._simul_spectra = spectra
            print(spectra.shape)
        else:
            self._simul_spectra = None