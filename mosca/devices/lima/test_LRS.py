#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Python script to test lima MOSCA spectro (without TangoDS)
"""

"""
usage example:

...
"""


import os
import pprint
import sys
import time


from LimaRoiSpectrum import LimaRoiSpectrum

def print_test(msg):
    print(f"[TEST_LRS] {msg}")


def trig_test():
    print("\n")
    print_test(" ------------------ TEST LimaRoiSpectrum mosca contreoller -----------")

    lrs = LimaRoiSpectrum()
    print_test(f"number of module(s): {lrs.number_modules}")

    lrs.lima_url = "id16b/roi2spectrum/zyla"
    print_test(f"Lima URL: {lrs.lima_url}")

    lrs.connect()

    print("---- lima info ------")
    pprint.pprint(lrs.get_lima_info())
    print("---------------------")


#    print_test(f"")

#    print_test(f"")

#    # PREPARE
#    points_count = 100
#    dev.capture_mode = "Trigger"
#    dev.trigger_mode = "Sync"
#    dev.number_points = points_count
#    dev.preset_value = 8
#    # NB: preset_cycle is adjusted automatically
#
#    # SET A ROI
#    print("Add Aula counter 55 88")
#    dev.addCounter(["Aula", "55", "88"])
#    print(dev.getCounterInfo("Aula"))
#
#    # START
#    lrs.start_acquisition()
#
#    # POLL
#    while dev.acq_nb_points < points_count:
#        time.sleep(0.3)
#        print(f"{dev.acq_nb_points}/{points_count}")
#
#    print(f"Acquiered {dev.acq_nb_points}/{points_count}")
#
#    # READ DATA
#    data = dev.data
#    data_length = len(data)
#
#    print(f"len(data)={data_length} ({points_count * dev.spectrum_size})")
#    print("data=", data)
#
#    # READ ROI (calculated by DS)
#    roi_data =
#    print("roi values =", roi_data)
#
    # END
    print_test("end of test\n\n")




def main(args):
    trig_test()

if __name__ == "__main__":
    main(sys.argv)
