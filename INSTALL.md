# MOSCA Installation

## From Conda

```shell
conda install mosca -c esrf-bcu
```

## From source

```shell
conda create -n mosca "python=3.8"
conda activate mosca

cd local/
git clone https://gitlab.esrf.fr/bliss/mosca mosca.git
cd mosca.git/
pip install -e .

```

## Extra dependencies to run Hamamatsu spectrometers

```shell
pip install pythonnet
```

## Extra dependencies to run OceanOptics spectrometers

```shell
pip install seabreeze
```






# Run server on a windows computer

# Run server on a linux computer
```shell
limaRoiSpectrum id16
```

# Installation of pyhandel and Tango server for FalconX

```
Install miniconda on Windows target
  (install for ALL users)

(recommended:  install "Microsoft Studio Code" if you plan to make changes )

Open miniconda powershell

Then:


mkdir c:\blissadm
cd c:\blissadm

mkdir falconx
mkdir falconx_config


falconx   contains official XIA software:  sitoro library, Prospect program

Install Prospect by clicking on the Prospect installer


mkdir falconx_config
# place config files in this

conda config --env --set channel_priority false
conda config --env --add channels conda-forge
conda config --env --append channels esrf-bcu

conda create -n mosca python=3.8 bliss pytango git mosca
conda activate mosca
```

# For FalconX

```
pip install cffi

cd falconx_tango

# test pyhandel
conda install matplotlib

test:
   python test_pyhandel.py


# un Tango server from command line
set TANGO_HOST environment variable 
   open windows settings
   search "environment" -> "Edit environment variables for your account"

open anaconda powershell

# run Tango server
Copy file falconxtango.ps1-template to falconxtango.ps1

(edit falconxtango.ps1 with Notepad or other (microsoft studio code recommended)) 
   Set values for:
      TANGO_HOST

   eventually for BEACON_HOST (but has no effect)

   modify line:
      python FalconX.py  <instance-name>

   check values for:
      conda environment
      handel-sitoro dll location

   copy falconx-start.lnk to Desktop # link to start server
   # check eventually that properties (in particular the  execution path) of this link are correct 

   prepare instance in jive
      set properties for device
          mandatory:
           config_dir    c:\\blissadm\\falconx_config  # for example
           lib_dir       c:\\blissadm\\falconx\\handel-sitoro-fxn-1.1.22-x64\\lib  # for example

      other possible properties:
           config_file  <your favourite config file>  # default config.ini , files must be contained in lib_dir defined above
           lib_name    handel.dll  # default is handel.dll.  dll file must be in lib_dir directory defined abover

      following are possible but ignored for now:
           gate_ignore  True
           gate_master  0
           sync_master  0
           input_polairty  1
```



## Example of tango resources for Lima Mosca

```yaml
LimaRoiSpectrum/zyla/DEVICE/LimaRoiSpectrum: "id42/mosca/zyla"
id42/mosca/zyla->default_exposure_time: 90
id42/mosca/zyla->default_readout_time: 10
id42/mosca/zyla->detname: zyla
```




# Conda packaging

```shell
conda install conda-build

conda build ./conda --prefix-length=80 --output-folder=dist/ -c conda-forge
```

# Conda upload

```shell
conda install anaconda-client

anaconda upload -u esrf-bcu --force dist/noarch/mosca-*.tar.bz2
```
