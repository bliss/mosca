import time
import gevent
import numpy as np
import pytest


@pytest.fixture
def mosca_simulator_with_data(mosca_simulator):
    return mosca_simulator


def wait_acq_done(device, timeout=5, r_pixels=None):
    state, read_pixel, saved_pixel, curr_pixel = device.getAcqStatus()
    start_t = time.monotonic()
    # state: 0=READY, 1=RUNNING, 2=FAULT
    # read_pixel: available number of pixels (i.e. taken out from device internal buffer)
    # saved_pixel: number of pixels saved by MOSCA server (usually zero in BLISS usage context)
    # curr_pixel: number of pixels acquired by the device (some pixels could still be in the device internal buffer)
    while state == 1 and (r_pixels or read_pixel == r_pixels):
        if time.monotonic() - start_t > timeout:
            raise TimeoutError("Timeout while waiting for AcqReady state.")
        gevent.sleep(0.5)
        state, read_pixel, saved_pixel, curr_pixel = device.getAcqStatus()


# TODO: test buffer states at startup

# TODO: put the rois in the simulator setup to avoid code copy
def test_simu_getdata_step(mosca_simulator):
    ms = mosca_simulator

    spectrum_size = 2048
    n_channels = 6
    n_pixels = 1

    spectra = np.random.randint(1, 1000000, size=(n_channels, n_pixels, spectrum_size), dtype=np.uint32)

    assert mosca_simulator.spectrum_size == 1024

    mosca_simulator.spectrum_size = spectrum_size
    assert mosca_simulator.spectrum_size == 2048

    mosca_simulator.setNumberModules(n_channels)
    assert mosca_simulator.number_channels == n_channels

    ms.inject_simul_spectra(spectra.flatten())


    def _to_cargs(pars):
        if len(pars) == 5:
            pars = [pars[0], f"{pars[1]}-{pars[2]}", f"{pars[3]}", f"{pars[4]}"]
        else:
            pars = [pars[0], f"{pars[1]}", f"{pars[2]}", f"{pars[3]}"]
        print("======", pars)
        return pars
    
        
    c1pars = ["count1", -1, 300, 495]
    c2pars = ["count2", 3, 550, 595]
    c3pars = ["count3", 0, n_channels, 300, 495]
    c4pars = ["count4", 2, 3, 700, 745]
    
    ms.addCounter(_to_cargs(c1pars))
    ms.addCounter(_to_cargs(c2pars))
    ms.addCounter(_to_cargs(c3pars)) # this one should be equal to counter 1!
    ms.addCounter(_to_cargs(c4pars))

    # testing single point acquisition
    ms.write_attribute("number_points", n_pixels)
    ms.startAcq()
    wait_acq_done(ms, r_pixels=1)
    data = ms.getData([0]).reshape(n_channels, 1, spectrum_size)
    expected = spectra[:, 0:1, :]
    assert np.array_equal(data, expected)

    # testing step (1 point)
    counters = ms.getCounterValues([0]).reshape(-1, n_pixels)

    expected_c1 = data[:, :, 300:496].sum(axis=(0, 2))
    assert np.array_equal(counters[0, :], expected_c1)
    expected_c2 = data[3:4, :, 550:596].sum(axis=(0, 2))
    assert np.array_equal(counters[1, :], expected_c2)
    # this one should be equal to counter1
    expected_c3 = data[:, :, 300:496].sum(axis=(0, 2))
    assert np.array_equal(counters[2, :], expected_c3)
    assert np.array_equal(counters[2, :], counters[0, :])
    expected_c4 = data[2:4, :, 700:746].sum(axis=(0, 2))
    assert np.array_equal(counters[3, :], expected_c4)


def test_simu_getdata_map(mosca_simulator):
    ms = mosca_simulator

    spectrum_size = 2048
    n_channels = 6
    n_pixels = 10

    spectra = np.random.randint(1, 1000000, size=(n_channels, n_pixels, spectrum_size), dtype=np.uint32)

    assert mosca_simulator.spectrum_size == 1024

    mosca_simulator.spectrum_size = spectrum_size
    assert mosca_simulator.spectrum_size == 2048

    mosca_simulator.setNumberModules(n_channels)
    assert mosca_simulator.number_channels == n_channels

    ms.inject_simul_spectra(spectra.flatten())

    # testing mapping
    ms.write_attribute("number_points", n_pixels)
    ms.startAcq()
    wait_acq_done(ms, r_pixels=n_pixels)
    data = ms.getData([0]).reshape(n_channels, n_pixels, spectrum_size)
    expected = spectra[:, 0:n_pixels, :]
    assert np.array_equal(data, expected)

    c1pars = ["count1", -1, 300, 495]
    c2pars = ["count2", 3, 550, 595]
    c3pars = ["count3", 0, n_channels, 300, 495]
    c4pars = ["count4", 2, 3, 700, 745]

    def _to_cargs(pars):
        if len(pars) == 5:
            pars = [pars[0], f"{pars[1]}-{pars[2]}", f"{pars[3]}", f"{pars[4]}"]
        else:
            pars = [pars[0], f"{pars[1]}", f"{pars[2]}", f"{pars[3]}"]
        print("======", pars)
        return pars

    ms.addCounter(_to_cargs(c1pars))
    ms.addCounter(_to_cargs(c2pars))
    ms.addCounter(_to_cargs(c3pars)) # this one should be equal to counter 1!
    ms.addCounter(_to_cargs(c4pars))

    ms.startAcq()
    wait_acq_done(ms, r_pixels=n_pixels)
    data = ms.getData([0]).reshape(n_channels, n_pixels, spectrum_size)
    expected = spectra[:, 0:n_pixels, :]
    assert np.array_equal(data, expected)

    counters = ms.getCounterValues([0]).reshape(-1, n_pixels)

    expected_c1 = data[:, :, 300:496].sum(axis=(0, 2))
    assert np.array_equal(counters[0, :], expected_c1)
    expected_c2 = data[3:4, :, 550:596].sum(axis=(0, 2))
    assert np.array_equal(counters[1, :], expected_c2)
    # this one should be equal to counter1
    expected_c3 = data[:, :, 300:496].sum(axis=(0, 2))
    assert np.array_equal(counters[2, :], expected_c3)
    assert np.array_equal(counters[2, :], counters[0, :])
    expected_c4 = data[2:4, :, 700:746].sum(axis=(0, 2))
    assert np.array_equal(counters[3, :], expected_c4)