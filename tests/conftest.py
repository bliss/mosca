import os
import sys
import socket
import psutil
import subprocess
from contextlib import contextmanager
import gevent

import pytest
from tango import DevState, DevFailed, DevError, DeviceProxy


def eprint(*args):
    print(*args, file=sys.stderr, flush=True)


def get_open_ports(n):
    sockets = [socket.socket() for _ in range(n)]
    try:
        for s in sockets:
            s.bind(("", 0))
        return [s.getsockname()[1] for s in sockets]
    finally:
        for s in sockets:
            s.close()


@pytest.fixture
def tango_host():
    tango_port = get_open_ports(1)[0]
    tg_host = f"localhost:{tango_port}"
    os.environ["TANGO_HOST"] = tg_host
    yield tg_host


@contextmanager
def mosca_simulator_context(personal_name, device_name):
    tg_host = os.environ['TANGO_HOST']
    host, port = tg_host.split(":")
    fqdn_prefix = f"tango://{tg_host}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"

    conda_env = os.environ.get("MOSCA_SIMULATOR_CONDA_ENV", "mosca")
    if not conda_env:
        conda_env = None
    conda = os.environ.get("CONDA_EXE", None)
    if conda_env and conda:
        if os.sep in conda_env:
            option = "-p"
        else:
            option = "-n"
        runner = [
            conda,
            "run",
            option,
            conda_env,
            "--no-capture-output",
            "SimulSpectro",
            "--nodb",
            f"-host", host,
            f"-port", port,
            f"-dlist", device_name
        ]
    else:
        runner = ["SimulSpectro"]

    with start_tango_server(
        *runner,
        personal_name,
        # "-v4",               # to enable debug
        device_fqdn=device_fqdn,
        # admin_device_fqdn=admin_device_fqdn,
        state=None,
        check_children=conda_env is not None,
        no_db=True,
        wait_db=False
    ) as dev_proxy:
        yield dev_proxy


@pytest.fixture
def mosca_simulator(tango_host):
    with mosca_simulator_context(
        "mosca_simulator", "id00/mosca/simulator"
    ) as fqdn_proxy:
        yield fqdn_proxy


def is_devfailed_reconnect_delayed(e):
    """`DevFailed` with reason "connection delayed" in the exception chain?

    :param Exception e:
    :return bool:
    """
    err = find_deverror(e, 0)
    return (
        err.reason == "API_CantConnectToDevice"
        and "The connection request was delayed" in err.desc
    )


def find_in_exception_chain(e, cls):
    """Find the first occurance of an exception type
    in an exception chain.

    :param Exception e:
    :param Exception cls:
    :returns Exception or None: exception of type `cls`
    """
    while not isinstance(e, cls):
        try:
            e = e.__cause__
        except AttributeError:
            e = None
            break
    return e


def find_deverror(e, level):
    """Find the first occurance of `DevFailed` in an exception chain
    and return the DevError at the specifed level. Returns an empy
    `DevError` when `DevFailed` not in the exception chain.

    :param Exception e:
    :param int level:
    :returns DevError:
    """
    # https://tango-controls.github.io/cppTango-docs/except.html
    try:
        return find_in_exception_chain(e, DevFailed).args[level]
    except (AttributeError, IndexError):
        return DevError()


def wait_tango_device(
    device_fqdn=None,
    admin_device_fqdn=None,
    state=DevState.ON,
    no_db=True,
    wait_db=True,
    timeout=10,
    timeout_msg=None,
):
    """Wait until the tango device comes online and return a proxy.

    :param str device_fqdn: tango device URL for ping and state checking
    :param str admin_device_fqdn: wait for this device before `device_fqdn`
    :param DevState state: required tate to be considered online (default: ON)
    :param num timeout:
    :param str timeout_msg:
    :param bool wait_db: wait for the tango database to go online
    :returns DeviceProxy:
    """
    db_err_msg = f"Cannot create a proxy to {device_fqdn} (Tango database offline?)"
    if not timeout_msg:
        timeout_msg = f"{device_fqdn} is not running"
    exception = None
    err_msg = None
    try:
        with gevent.Timeout(timeout):
            # Wait until the admin device and optionally the database are online
            if admin_device_fqdn:
                wait_tango_device(
                    device_fqdn=admin_device_fqdn,
                    wait_db=wait_db,
                    state=None,
                    timeout=None,
                    timeout_msg=f"Admin device {admin_device_fqdn} is not running",
                )
                wait_db = False

            # Wait until the device and optionally the database are online
            while True:
                # Check whether database is online.
                try:
                    if no_db:
                        device_fqdn = f"{device_fqdn}#dbase=no"
                    dev_proxy = DeviceProxy(device_fqdn)
                except DevFailed as e:
                    err_msg = db_err_msg
                    if wait_db:
                        exception = e
                        gevent.sleep(0.1)
                    else:
                        raise
                else:
                    # Database is online. Now check whether the device is online.
                    err_msg = timeout_msg
                    try:
                        dev_proxy.ping()
                        break
                    except DevFailed as e:
                        if not is_devfailed_reconnect_delayed(e):
                            exception = e
                            gevent.sleep(0.1)
                            continue

                    # Each DeviceProxy keeps track of the time since its last failure
                    # to connect. A connection attempt within 1 second of the last failure
                    # results in a "Reconnection delayed" exception.
                    #
                    # It seems that when DeviceProxy is instantiated, a failed connection
                    # attempt may have already happened. In this case `ping` will raise a
                    # "Reconnection delayed" exception. So sleep and try again:
                    gevent.sleep(1.1)
                    try:
                        dev_proxy.ping()
                        break
                    except DevFailed as e:
                        exception = e

            # Device and database are online. Now check the device state.
            if state is not None:
                if timeout_msg:
                    err_msg = f"{timeout_msg}: {device_fqdn} not in {state} state"
                else:
                    err_msg = f"{device_fqdn} not in {state} state"
                try:
                    actual_state = None
                    while actual_state != state:
                        actual_state = dev_proxy.state()
                        gevent.sleep(0.1)
                except gevent.Timeout:
                    if timeout_msg:
                        err_msg = f"{timeout_msg}: {device_fqdn} is {actual_state} instead of {state}"
                    else:
                        err_msg = f"{device_fqdn} is {actual_state} instead of {state}"
                    raise
    except gevent.Timeout:
        if err_msg is None:
            err_msg = timeout_msg
        raise RuntimeError(err_msg) from exception
    return dev_proxy


@contextmanager
def start_tango_server(*cmdline_args, check_children=False, **kwargs):
    """
    Arguments:
        check_children: If true, children PID are also checked during the terminating
    """
    device_fqdn = kwargs["device_fqdn"]
    exception = None
    for _ in range(3):
        print("==========", cmdline_args)
        p = subprocess.Popen(cmdline_args)
        try:
            dev_proxy = wait_tango_device(**kwargs)
        except Exception as e:
            exception = e
            wait_terminate(p)
        else:
            break
    else:
        raise RuntimeError(f"could not start {device_fqdn}."
                           " Did you set the MOSCA_SIMULATOR_CONDA_ENV env var "
                           "to the right conda environment?") from exception

    try:
        # FIXME: This have to be cleaned up by returning structured data
        # Expose the server PID as a proxy attribute
        object.__setattr__(dev_proxy, "server_pid", p.pid)
        yield dev_proxy
    finally:
        wait_terminate(p, check_children=check_children)


def wait_terminate(process, timeout=10, check_children=False):
    """
    Try to terminate a process then kill it.

    This ensure the process is terminated.

    Arguments:
        process: A process object from `subprocess` or `psutil`, or an PID int
        timeout: Timeout to way before using a kill signal
        check_children: If true, check children pid and force there termination
    Raises:
        gevent.Timeout: If the kill fails
    """
    children = []
    if isinstance(process, int):
        try:
            name = str(process)
            process = psutil.Process(process)
        except Exception:
            # PID is already dead
            return
    else:
        name = repr(" ".join(process.args))
        if process.poll() is not None:
            eprint(f"Process {name} already terminated with code {process.returncode}")
            return

    if check_children:
        if not isinstance(process, psutil.Process):
            process = psutil.Process(process.pid)
        children = process.children(recursive=True)

    process.terminate()
    try:
        with gevent.Timeout(timeout):
            # gevent timeout have to be used here
            # See https://github.com/gevent/gevent/issues/622
            process.wait()
    except gevent.Timeout:
        eprint(f"Process {name} doesn't finish: try to kill it...")
        process.kill()
        with gevent.Timeout(10):
            # gevent timeout have to be used here
            # See https://github.com/gevent/gevent/issues/622
            process.wait()

    if check_children:
        for i in range(10):
            _done, alive = psutil.wait_procs(children, timeout=1)
            if not alive:
                break
            for p in alive:
                try:
                    if i < 3:
                        p.terminate()
                    else:
                        p.kill()
                except psutil.NoSuchProcess:
                    pass
        else:
            raise RuntimeError(
                "Timeout expired after 10 seconds. Process %s still alive." % alive
            )
