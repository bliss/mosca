rem @echo OFF
rem How to run a Python script in a given conda environment from a batch file.

rem It doesn't require:
rem - conda to be in the PATH
rem - cmd.exe to be initialized with conda init

rem Define here the path to your conda installation
set CONDAPATH=C:\Users\opid00\AppData\Local\miniconda3\
set ACTIVATE=%CONDAPATH%\Scripts\activate.bat

rem Define here the name of the environment
set ENVNAME=mosca

rem The following command activates the base environment.
rem call C:\ProgramData\Miniconda3\Scripts\activate.bat C:\ProgramData\Miniconda3
rem if %ENVNAME%==base (set ENVPATH=%CONDAPATH%) else (set ENVPATH=%CONDAPATH%\envs\%ENVNAME%)

rem Activate the conda environment
call %ACTIVATE% %ENVNAME%

rem Run a python script in that environment
rem  you can either pass a parameter to the server (to be started by Starter)
rem  or fix your own instance-name instead if you want to start it by hand
rem cd c:\blissadm\falconx_tango
HamaSpectro.exe txo



